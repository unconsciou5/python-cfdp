import zmq
from bitarray import bitarray
from bitarray.util import ba2int


class PduHeader:

    def __init__(
            self,
            pdu_type,
            direction,
            transmission_mode,
            source_entity_id,
            transaction_id,
            destination_entity_id,
            crc_flag=0,
            pdu_data_field_length=0,
            length_of_entity_ids=0,
            length_of_transaction_id=0,
            version=0
            ):
        self.version = 0
        self.pdu_type = pdu_type
        self.direction = direction
        self.transmission_mode = transmission_mode
        self.crc_flag = crc_flag
        self.pdu_data_field_length = pdu_data_field_length
        self.length_of_entity_ids = length_of_entity_ids
        self.length_of_transaction_id = length_of_transaction_id
        self.source_entity_id = source_entity_id
        self.transaction_id = transaction_id
        self.destination_entity_id = destination_entity_id

    def from_bytes(pdu_header_buffer):
        pdu_header_array = bitarray(endian='big')
        pdu_header_array.frombytes(pdu_header_buffer)
        length_of_entity_ids = ba2intM(pdu_header_array[25:28])
        length_of_transaction_id = ba2intM(pdu_header_array[29:32])
        fixed_pdu_end = 32 + (2*(8 + 8*length_of_entity_ids)) + \
            (8 + 8*length_of_transaction_id)
        pdu_header_received = PduHeader(
            version=ba2intM(pdu_header_array[:3]),
            pdu_type=pdu_header_array[3],
            direction=pdu_header_array[4],
            transmission_mode=pdu_header_array[5],
            crc_flag=pdu_header_array[6],
            pdu_data_field_length=ba2intM(pdu_header_array[8:24]),
            length_of_entity_ids=length_of_entity_ids,
            length_of_transaction_id=length_of_transaction_id,
            source_entity_id=ba2intM(
                pdu_header_array[32:(32+(8+8*length_of_entity_ids))]
                ),
            transaction_id=ba2intM(
                pdu_header_array[
                    (32 + (8 + 8*length_of_entity_ids)):
                    (32+(8+8*length_of_entity_ids) +
                        (8 + 8*length_of_transaction_id))
                    ]
                ),
            destination_entity_id=ba2intM(
                pdu_header_array[
                    (
                        32 + (8 + 8*length_of_entity_ids) +
                        (8 + 8*length_of_transaction_id)
                    ):(fixed_pdu_end)]
                )
            )
        return pdu_header_received


class MetadataPdu:

    def __init__(
            self,
            header,
            file_directive=0x7,
            segmentation_control=0,
            file_size=None,
            length_of_source_file_name=None,
            source_file_name=None,
            length_of_destination_file_name=None,
            destination_file_name=None,
            type_of_TLV=None,
            length_of_TLV=None,
            value_of_TLV=None
            ):

        self.header = header
        self.file_directive = file_directive
        self.segmentation_control = segmentation_control
        self.file_size = file_size
        self.source_file_name = source_file_name
        self.destination_file_name = destination_file_name
        self.type_of_TLV = type_of_TLV
        self.length_of_TLV = length_of_TLV
        self.value_of_TLV = value_of_TLV

    def from_bytes(metadata_buffer):
        metadata_array = bitarray(endian='big')
        metadata_array.frombytes(metadata_buffer)
        header = PduHeader.from_bytes(metadata_buffer)
        length_of_entity_ids = ba2intM(metadata_array[25:28])
        length_of_transaction_id = ba2intM(metadata_array[29:32])
        fixed_pdu_end = 32 + (2*(8+8*length_of_entity_ids)) + \
            (8+8*length_of_transaction_id)
        length_of_source_file_name = ba2intM(
            metadata_array[(fixed_pdu_end+48):(fixed_pdu_end+56)])
        length_of_destination_file_name = ba2intM(
            metadata_array[
                (fixed_pdu_end+56+(8*length_of_source_file_name)):
                (fixed_pdu_end+64+(8*length_of_source_file_name))])
        end_of_destination_file_name = fixed_pdu_end + 64 + \
            (8*length_of_source_file_name) + 8*length_of_destination_file_name
        length_of_TLV = ba2intM(
            metadata_array[
                (end_of_destination_file_name+8):
                (end_of_destination_file_name+16)])

        MetadataPdu_received = MetadataPdu(
            header=header,
            file_directive=ba2intM(
                metadata_array[fixed_pdu_end:(fixed_pdu_end+8)]),
            segmentation_control=metadata_array[(fixed_pdu_end+8)],
            file_size=ba2intM(
                metadata_array[(fixed_pdu_end+16):(fixed_pdu_end+48)]),
            length_of_source_file_name=length_of_source_file_name,
            source_file_name=bitarray.tostring(metadata_array[
                (fixed_pdu_end+56):
                (fixed_pdu_end+56+(8*length_of_source_file_name))]),
            length_of_destination_file_name=length_of_destination_file_name,
            destination_file_name=bitarray.tostring(metadata_array[
                (fixed_pdu_end+56+8+(8*length_of_source_file_name)):
                (end_of_destination_file_name)]),
            type_of_TLV=ba2intM(metadata_array[
                end_of_destination_file_name:
                (end_of_destination_file_name+8)]),
            length_of_TLV=length_of_TLV,
            value_of_TLV=metadata_array[
                (end_of_destination_file_name+16):
                (end_of_destination_file_name+16+8*length_of_TLV)]
        )
        return MetadataPdu_received


class FiledataPdu:

    def __init__(
            self,
            header=PduHeader,
            segment_offset=None,
            file_data=None
            ):
        self.header = header
        self.segment_offset = segment_offset
        self.file_data = file_data

    def from_bytes(filedata_buffer):
        filedata_array = bitarray(endian='big')
        filedata_array.frombytes(filedata_buffer)
        header = PduHeader.from_bytes(filedata_buffer)
        length_of_pdu_data_field = header.pdu_data_field_length
        length_of_entity_ids = ba2intM(filedata_array[25:28])
        length_of_transaction_id = ba2intM(filedata_array[29:32])
        fixed_pdu_end = 32 + (2*(8+8*length_of_entity_ids)) + \
            (8+8*length_of_transaction_id)
        file_data = filedata_array[
                (fixed_pdu_end+32):
                (fixed_pdu_end+32+length_of_pdu_data_field*8)]
        FiledataPdu_received = FiledataPdu(
            header=header,
            segment_offset=ba2intM(
                filedata_array[fixed_pdu_end:(fixed_pdu_end+32)]),
            file_data=file_data.tobytes()
            )
        return FiledataPdu_received


class EOFPdu:

    def __init__(
            self,
            header=PduHeader,
            file_directive=0x4,
            condition_code=None,
            file_checksum=None,
            file_size=None,
            TLV=None
            ):
        self.header = header
        self.header.pdu_type = 0
        self.file_directive = file_directive
        self.condition_code = condition_code
        self.file_checksum = file_checksum
        self.file_size = file_size
        self.TLV = TLV

    def from_bytes(EOF_buffer):
        EOF_array = bitarray(endian='big')
        EOF_array.frombytes(EOF_buffer)
        header = PduHeader.from_bytes(EOF_buffer)
        length_of_entity_ids = ba2intM(EOF_array[25:28])
        length_of_transaction_id = ba2intM(EOF_array[29:32])
        fixed_pdu_end = 32 + (2*(8+8*length_of_entity_ids)) + \
            (8+8*length_of_transaction_id)
        EOF_received = EOFPdu(
            header=header,
            condition_code=ba2intM(
                EOF_array[(fixed_pdu_end+8):(fixed_pdu_end+12)]),
            file_checksum=ba2intM(
                EOF_array[(fixed_pdu_end+16):(fixed_pdu_end+48)]),
            file_size=ba2intM(
                EOF_array[(fixed_pdu_end+48):(fixed_pdu_end+80)])
            )
        return EOF_received


def ba2intM(bitarray_to_check):
    if bitarray_to_check == bitarray(''):
        return 0
    else:
        return ba2int(bitarray_to_check)


def identify(pdu_input):
    data_array = bitarray(endian='big')
    data_array.frombytes(pdu_input)
    length_of_entity_ids = ba2intM(data_array[25:28])
    length_of_transaction_id = ba2intM(data_array[29:32])
    fixed_pdu_end = 32 + (2*(8 + 8*length_of_entity_ids)) + \
        (8 + 8*length_of_transaction_id)
    return ba2intM(data_array[fixed_pdu_end:(fixed_pdu_end+8)])


context = zmq.Context()
socket = context.socket(zmq.PAIR)
socket.bind("tcp://*:5557")

print("<------------------------------ fixed Header ------------------------------------>")
print("──┬──────────┬───────────┬────────┬─────┬─────────────┬────────┬────────┬────────┐┌─────────┬─────────────────────────┐")
print("V │ PDU Type │ Direction │ T.Mode │ CRC │ Data length │SourceID│ Tr.Nr. │Dest. ID││  Type   │     specific Content    │")
print("──┼──────────┼───────────┼────────┼─────┼─────────────┼────────┼────────┼────────┤├─────────┼─────────────────────────┘")

while True:
    pdu_buffer = socket.recv()
    header = PduHeader.from_bytes(pdu_buffer)

    type = identify(pdu_buffer)
    if type == 7:
        type = "Metadata"
        Metadata_PDU = MetadataPdu.from_bytes(pdu_buffer)
        specific_content = (
            str(Metadata_PDU.source_file_name) + " | " +
            str(Metadata_PDU.destination_file_name) + " | " +
            str(Metadata_PDU.file_size) + " Bytes" + " | " +
            "S.Control: " + str(Metadata_PDU.segmentation_control) +
            " | " + "TLV: " + str(Metadata_PDU.type_of_TLV)
            )
    if type == 4:
        type = "  EOF   "
        EOF_PDU = EOFPdu.from_bytes(pdu_buffer)
        specific_content = (
            "Checksum: " + str(EOF_PDU.file_checksum) +
            " | Filesize: " + str(EOF_PDU.file_size) + " Bytes" +
            " | ConditionCode: " + str(EOF_PDU.condition_code) +
            " | TLV: " + str(EOF_PDU.TLV)
            )
    if header.pdu_type:
        header.pdu_type = "Data     "
        type = "  File  "
        Data_PDU = FiledataPdu.from_bytes(pdu_buffer)
        specific_content = "offset: " + str(Data_PDU.segment_offset)
    else:
        header.pdu_type = "Directive"

    if header.direction:
        header.direction = "to Sender"
    else:
        header.direction = "to Receiv"

    if header.transmission_mode:
        header.transmission_mode = "unack"
    else:
        header.transmission_mode = "ack  "

    if header.crc_flag:
        header.crc_flag = "yes"
    else:
        header.crc_flag = "no "

    print(
        str(header.version) + " │ " + str(header.pdu_type) + "│ " +
        str(header.direction) + " │ " + str(header.transmission_mode) + "  │ "
        + str(header.crc_flag) + " │ " +
        format(header.pdu_data_field_length, '011') + " │ " +
        format(header.source_entity_id, '06') + " │ " +
        format(header.transaction_id, '06') + " │ " +
        format(header.destination_entity_id, '06') + " ││ " + str(type) + "│ "
        + specific_content
        )
