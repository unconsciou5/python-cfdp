import logging; logging.basicConfig(level=logging.DEBUG)
import time

import cfdp
from config import config


cfdp_user = cfdp.CfdpUser(
    entity_id=2,
    transmission_mode=cfdp.TransmissionMode.UNRELIABLE
)

cfdp_user.bind("tcp://*:5557")
cfdp_user.listen()


# cfdp_user.shutdown()
