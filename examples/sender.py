import logging; logging.basicConfig(level=logging.DEBUG)
import time

import cfdp
from cfdp.transport import ZmqTransport
from config import config


transport = ZmqTransport()
transport.connect("tcp://localhost:5557")


cfdp_sender = cfdp.CfdpSender(
    entity_id=1,
    transport=transport,
    config=config,
)

try:
    transaction_id = cfdp_sender.put_request(
        destination_id=2,
        source_filename="files/medium.txt",
        destination_filename="files/medium_received.txt")
    # this should be a class method:
    while transaction_id in cfdp_sender._machines:
        time.sleep(0)

except KeyboardInterrupt:
    pass

finally:
    cfdp_sender.shutdown()
    transport.disconnect()
    transport.shutdown()
