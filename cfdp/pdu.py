from bitarray import bitarray

from .enums import FileDirectiveCode, PduTypeCode


class PduHeader:

    def __init__(
            self,
            pdu_type,
            direction,
            transmission_mode,
            source_entity_id,
            transaction_id,
            destination_entity_id,
            crc_flag=0,
            pdu_data_field_length=0,
            length_of_entity_ids=0,
            length_of_transaction_id=0
            ):
        self.version = 0
        self.pdu_type = pdu_type
        self.direction = direction
        self.transmission_mode = transmission_mode
        self.crc_flag = crc_flag
        self.pdu_data_field_length = pdu_data_field_length
        self.length_of_entity_ids = length_of_entity_ids
        self.length_of_transaction_id = length_of_transaction_id
        self.source_entity_id = source_entity_id
        self.transaction_id = transaction_id
        self.destination_entity_id = destination_entity_id

    def to_bytes(self):
        databits = bitarray(
            format(self.version, '03b') +
            format(self.pdu_type, '01b') +
            format(self.direction, '01b') +
            format(self.transmission_mode, '01b') +
            format(self.crc_flag, '01b') +
            format(0, '01b') +  # reserved
            format(self.pdu_data_field_length, '016b') +
            format(0, '01b') +  # reserved
            format(self.length_of_entity_ids, '03b') +
            format(0, '01b') +  # reserved
            format(self.length_of_transaction_id, '03b') +
            format(self.source_entity_id, '0' +
                   str(8 + self.length_of_entity_ids * 8) + 'b') +
            format(self.transaction_id, '0' +
                   str(8 + self.length_of_transaction_id * 8) + 'b') +
            format(self.destination_entity_id, '0' +
                   str(8 + self.length_of_entity_ids * 8) + 'b'))
        return databits.tobytes()


class MetadataPdu:

    def __init__(
            self,
            direction,
            transmission_mode,
            transaction,
            segmentation_control=False,
            *args, **kwargs
            ):
        self.file_directive_code = FileDirectiveCode.METADATA.value
        self.segmentation_control = segmentation_control
        self.source_filename = transaction.source_filename
        self.destination_filename = transaction.destination_filename

        self.file_size = transaction.get_file_size()

        self.pdu_header = PduHeader(
            pdu_type=PduTypeCode.FILE_DIRECTIVE.value,
            direction=direction.value,
            transmission_mode=transmission_mode.value,
            source_entity_id=transaction.source_entity_id,
            transaction_id=transaction.id,
            destination_entity_id=transaction.destination_entity_id,
            *args, **kwargs
            )

    def to_bytes(self):
        databits = bitarray(
            format(self.file_directive_code, '08b') +
            format(self.segmentation_control, '01b') +
            format(0, '07b') +
            format(self.file_size, '032b')
            )
        if self.source_filename:
            value = self.source_filename.encode('utf-8')
            length = len(value)
            databits += bitarray(format(length, '08b'))
            databits.frombytes(value)
        else:
            databits += bitarray(format(0, '08b'))
        if self.destination_filename:
            value = self.destination_filename.encode('utf-8')
            length = len(value)
            databits += bitarray(format(length, '08b'))
            databits.frombytes(value)
        else:
            databits += bitarray(format(0, '08b'))
        databytes = databits.tobytes()
        self.pdu_header.pdu_data_field_length = len(databytes)
        return self.pdu_header.to_bytes() + databytes


class EOFPdu:

    def __init__(
            self,
            direction,
            transmission_mode,
            transaction,
            condition_code,
            *args, **kwargs
            ):
        self.file_directive_code = FileDirectiveCode.EOF.value
        self.condition_code = condition_code.value

        self.file_checksum = transaction.get_file_checksum()
        self.file_size = transaction.get_file_size()

        self.pdu_header = PduHeader(
            pdu_type=PduTypeCode.FILE_DIRECTIVE.value,
            direction=direction.value,
            transmission_mode=transmission_mode.value,
            source_entity_id=transaction.source_entity_id,
            transaction_id=transaction.id,
            destination_entity_id=transaction.destination_entity_id,
            *args, **kwargs
            )

    def to_bytes(self):
        databits = bitarray(
            format(self.file_directive_code, '08b') +
            format(self.condition_code, '04b') +
            format(0, '04b') +
            format(self.file_checksum, '032b') +
            format(self.file_size, '032b')
            )
        databytes = databits.tobytes()
        self.pdu_header.pdu_data_field_length = len(databytes)
        return self.pdu_header.to_bytes() + databytes


class FiledataPdu:

    def __init__(
            self,
            direction,
            transmission_mode,
            transaction,
            segment_offset,
            file_data,
            *args, **kwargs
            ):
        self.segment_offset = segment_offset
        self.file_data = file_data

        self.pdu_header = PduHeader(
            pdu_type=PduTypeCode.FILE_DATA.value,
            direction=direction.value,
            transmission_mode=transmission_mode.value,
            source_entity_id=transaction.source_entity_id,
            transaction_id=transaction.id,
            destination_entity_id=transaction.destination_entity_id,
            *args, **kwargs
            )

    def to_bytes(self):
        databits = bitarray(format(self.segment_offset, '032b'))
        databytes = databits.tobytes()
        databytes += self.file_data
        self.pdu_header.pdu_data_field_length = len(databytes)
        return self.pdu_header.to_bytes() + databytes
