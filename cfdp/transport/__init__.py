import zmq


class Transport:

    def __init__(self):
        self.protocol_entity = None

    def request(self, data, address=None):
        raise NotImplementedError

    def indication(self):
        pass

    def is_ready(self):
        # return True if communication is currently possible
        raise NotImplementedError


class ZmqTransport(Transport):

    def __init__(self):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.PAIR)
        self._ready = False

    def request(self, data, address=None):
        self.socket.send(data)

    def is_ready(self):
        return self._ready

    def connect(self, address):
        self.socket.connect(address)
        self._ready = True

    def disconnect(self):
        self.socket.close()
        self._ready = False

    def shutdown(self):
        self.context.destroy()

    # def bind(self, address):
    #     self.socket.bind(address)
    #
    # def receive(self):
    #     return self.socket.recv()
