import os
from cfdp import logger
from cfdp.enums import Role, MachineState, EventType, ConditionCode
from cfdp.pdu import PduHeader, MetadataPdu, FiledataPdu, EOFPdu


class TransactionError(Exception):
    pass


class Transaction:

    def __init__(
            self,
            id,
            source_entity_id,
            destination_entity_id,
            source_path,
            destination_path,
            file_size=None):
        self.id = id
        self.source_entity_id = source_entity_id
        self.destination_entity_id = destination_entity_id
        self.source_path = source_path
        self.destination_path = destination_path
        self.file_size = file_size

        self.file = None
        self.frozen = False
        self.suspended = False


class Event:

    def __init__(self, transaction, event_type):
        self.transaction = transaction
        self.event_type = event_type

    def __repr__(self):
        return "<Event({}, {})>".format(self.transaction, self.event_type)


class Receiver1:

    def __init__(self, kernel, transaction):
        self.kernel = kernel
        self.transaction = transaction
        self.role = Role.CLASS_1_RECEIVER
        self.state = MachineState.WAITING_FILE_PDU

    def _trigger_event(self, event_type):
        self.kernel.trigger_event(self.transaction, event_type)

    def update_state(self, event=None, pdu=None, request=None):

        if self.state == MachineState.WAITING_FILE_PDU:

            if event == EventType.RECEIVED_METADATA_PDU:
                logger.debug("Receiver {}: WAITING FILE PDU".format(
                    self.transaction.id))
                EOF_not_arrived = True
                file = open(self.transaction.destination_path, 'wb+')
                while EOF_not_arrived:
                    received_data = self.kernel.transport.receive()
                    received_header = PduHeader.from_bytes(received_data)
                    if received_header.pdu_type:
                        file_pdu = FiledataPdu.from_bytes(received_data)
                        file.seek(file_pdu.segment_offset, 0)
                        file.write(file_pdu.file_data)
                        # self._trigger_event(EventType.RECEIVED_FILEDATA_PDU)
                    else:
                        logger.debug("Receiver {}: EOF RECEIVED".format(
                            self.transaction.id))
                        EOF_not_arrived = False
                        file.close()
                        EOF_pdu = EOFPdu.from_bytes(received_data)
                        file_checksum = self._calculate_checksum()
                        if file_checksum == EOF_pdu.file_checksum:
                            self._trigger_event(
                                EventType.RECEIVED_EOF_NO_ERROR_PDU)
                        else:
                            logger.debug(
                                "Receiver {}: CHECKSUM INCORRECT".format(
                                    self.transaction.id))

                # FILE_SEGMENT_SIZE = len(file_pdu.file_data)
                # file_size = self.transaction.file_size
                # number_of_FD = file_size // FILE_SEGMENT_SIZE
                # if file_size % FILE_SEGMENT_SIZE > 0:
                #     number_of_FD = number_of_FD + 1
                # x = 1
                # print("Waiting for %s FilePDUs" % number_of_FD)
                # while x <= number_of_FD:
                #     received_data = self.kernel.transport.receive()
                #     file_pdu = FiledataPdu.from_bytes(received_data)
                #     file.seek(file_pdu.segment_offset, 0)
                #     file.write(file_pdu.file_data)
                #     x = x + 1

            # if event == EventType.RECEIVED_FILEDATA_PDU:
            #    logger.debug("Receiver {}: PDU RECEIVED".format(
            #        self.transaction.id))

            if event == EventType.RECEIVED_EOF_NO_ERROR_PDU:
                logger.debug("Receiver {}: CHECKSUM CORRECT".format(
                    self.transaction.id))
                # self.shutdown()

    def _calculate_checksum(self):
        logger.debug("Receiver {}: CALCULATING CHECKSUM".format(
            self.transaction.id))
        x = 0
        file_checksum = 0
        a = b''
        file = open(self.transaction.destination_path, "rb")
        while x < self.transaction.file_size:
            file.seek(x)
            if x > self.transaction.file_size - 4:
                a = file.read(self.transaction.file_size % 4)
                int_a = int(a.hex(), 16) * 2**(
                    (4 - self.transaction.file_size % 4)*8)
            else:
                a = file.read(4)
                int_a = int(a.hex(), 16)
            file_checksum = file_checksum + int_a
            x = x + 4
        file_checksum = file_checksum % 2**32
        return file_checksum

    def _issue_transaction(self):
        self.kernel.transaction_indication(self.transaction.id)

    def _issue_transaction_finished(self):
        self.kernel.transaction_finished_indication(self.transaction.id)

    def _is_file_transfer(self):
        return True  # self.transaction.file is not None

    def _shutdown(self):
        if self.transaction.file:
            self.transaction.file.close()
