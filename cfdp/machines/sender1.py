import os
from cfdp import logger
from cfdp.enums import MachineState, ConditionCode, Direction, TransmissionMode
from cfdp.event import Event, EventType
from cfdp.pdu import MetadataPdu, EOFPdu, FiledataPdu


class Sender1:

    def __init__(self, kernel, transaction):
        self.kernel = kernel
        self.transaction = transaction
        self.state = MachineState.SEND_METADATA
        self.transmission_mode = TransmissionMode.UNRELIABLE

    def _trigger_event(self, event_type):
        self.kernel._trigger_event(Event(self.transaction, event_type))

    def update_state(self, event=None):
        """ As per state table given in CCSDS 720.2-G-3, Table 5-1. """

        logger.debug("[{}] Event: {}".format(event.transaction.id, event.type))

        if self.state == MachineState.SEND_METADATA:

            if event.type == EventType.RECEIVED_PUT_REQUEST:
                self._issue_transaction_indication()

                if self._is_file_transfer():
                    logger.debug("[{}] Send Metadata".format(
                        event.transaction.id))
                    self._send_metadata()
                    self.state = MachineState.SEND_FILE
                    self._trigger_event(EventType.ENTERED_STATE)
                else:
                    logger.debug("[{}] Send Metadata".format(
                        event.transaction.id))
                    self._send_metadata()
                    self._send_EOF(ConditionCode.NO_ERROR)
                    self._issue_transaction_finished_indication()
                    self._shutdown()

            else:
                raise NotImplementedError

        elif self.state == MachineState.SEND_FILE:

            if event.type == EventType.ENTERED_STATE:
                if not os.path.isfile(self.transaction.source_filename):
                    raise Exception("Source file does not exist.")
                self.transaction.file_handle = open(
                    self.transaction.source_filename, 'rb')
                self._trigger_event(EventType.SEND_FILE_DATA)

            elif event.type == EventType.SEND_FILE_DATA:
                if not self.transaction.frozen\
                        and not self.transaction.suspended:
                    if self._is_comm_layer_ready():
                        logger.debug("[{}] Send Filedata".format(
                            event.transaction.id))
                        self._send_file_data()
                        if self._is_entire_file_sent():
                            logger.debug("[{}] Send EOF".format(
                                event.transaction.id))
                            self._send_EOF(ConditionCode.NO_ERROR)
                            self._issue_eof_sent_indication()
                            self._issue_transaction_finished_indication()
                            self._shutdown()
                            return
                    self._trigger_event(EventType.SEND_FILE_DATA)

            else:
                raise NotImplementedError

    def _issue_transaction_indication(self):
        self.kernel.transaction_indication(self.transaction.id)

    def _is_file_transfer(self):
        return self.transaction.source_filename is not None

    def _issue_transaction_finished_indication(self):
        self.kernel.transaction_finished_indication(self.transaction.id)

    def _issue_eof_sent_indication(self):
        self.kernel.eof_sent_indication(self.transaction.id)

    def _send_metadata(self):
        metadata_pdu = MetadataPdu(
            direction=Direction.TOWARD_RECEIVER,
            transmission_mode=self.transmission_mode,
            transaction=self.transaction
            )
        self.kernel.transport.request(metadata_pdu.to_bytes())

    def _send_EOF(self, condition_code):
        eof_pdu = EOFPdu(
            direction=Direction.TOWARD_RECEIVER,
            transmission_mode=self.transmission_mode,
            transaction=self.transaction,
            condition_code=condition_code
            )
        self.kernel.transport.request(eof_pdu.to_bytes())

    def _is_comm_layer_ready(self):
        return self.kernel.transport.is_ready()

    def _is_entire_file_sent(self):
        return self.transaction.is_file_send_complete()

    def _send_file_data(self):
        offset, data = self.transaction.get_file_segment()
        filedata_pdu = FiledataPdu(
            direction=Direction.TOWARD_RECEIVER,
            transmission_mode=self.transmission_mode,
            transaction=self.transaction,
            segment_offset=offset,
            file_data=data
            )
        self.kernel.transport.request(filedata_pdu.to_bytes())

    def _shutdown(self):
        if self.transaction.file_handle:
            self.transaction.file_handle.close()
            self.transaction.file_handle = None
