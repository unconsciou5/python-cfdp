import queue
import time
import threading

from . import logger
from .enums import TransmissionMode
from .event import Event, EventType
from .transaction import Transaction
from .machines import Sender1


class CfdpSender:

    def __init__(
            self,
            entity_id,
            transport,
            config=None):
        self.entity_id = entity_id
        self.config = config

        self._transaction_counter = 0
        self._machines = {}
        self._event_queue = queue.Queue()
        self._event_thread = threading.Thread(target=self._event_handler)
        self._event_thread.kill = False
        self._event_thread.start()

        self.transport = transport
        self.transport.protocol_entity = self

    def _increment_transaction_counter(self):
        self._transaction_counter += 1
        return self._transaction_counter

    def put_request(
            self,
            destination_id,
            source_filename,
            destination_filename=None,
            segmentation_control=None,
            fault_handler_overrides=None,
            flow_label=None,
            transmission_mode=TransmissionMode.UNRELIABLE,
            message_to_user=None,
            filestore_requests=None):

        transaction_id = self._increment_transaction_counter()
        transaction = Transaction(
            transaction_id,
            self.entity_id,
            destination_id,
            source_filename=source_filename,
            destination_filename=destination_filename)

        if transmission_mode == TransmissionMode.UNRELIABLE:
            machine = Sender1(self, transaction)
        else:
            raise NotImplementedError
        self._machines[transaction_id] = machine

        self._trigger_event(Event(transaction, EventType.RECEIVED_PUT_REQUEST))
        return transaction_id

    def report_request(self):
        pass

    def transaction_indication(self, transaction_id):
        logger.debug("[%s] Transaction indication" % transaction_id)

    def eof_sent_indication(self, transaction_id):
        logger.debug("[%s] EOF indication" % transaction_id)

    def transaction_finished_indication(self, transaction_id):
        self._machines.pop(transaction_id)
        logger.debug("[%s] Transaction finished indication" % transaction_id)

    def _trigger_event(self, event):
        self._event_queue.put(event)

    def _event_handler(self):
        thread = threading.currentThread()
        while True and not thread.kill:
            time.sleep(0)
            try:
                event = self._event_queue.get(block=False)
                machine = self._machines.get(event.transaction.id)
                if machine:
                    machine.update_state(event=event)
            except queue.Empty:
                pass

    def shutdown(self):
        self._event_thread.kill = True


class CfdpReceiver:

    def meta_received_indication(self):
        pass

    def file_segment_received_indication(self):
        pass

    def transcation_finished_indication(self):
        pass
