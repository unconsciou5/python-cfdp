import os


class Transaction:

    FILE_SEGMENT_SIZE = 64

    def __init__(
            self,
            id,
            source_entity_id,
            destination_entity_id,
            source_filename,
            destination_filename):
        self.id = id
        self.source_entity_id = source_entity_id
        self.destination_entity_id = destination_entity_id
        self.source_filename = source_filename
        self.destination_filename = destination_filename

        self.file_handle = None
        self.frozen = False
        self.suspended = False

        self._file_checksum = None
        self._file_size = None
        self._file_seek = 0

    def get_file_size(self):
        if self._file_size is None:
            self._calculate_file_size()
        return self._file_size

    def get_file_checksum(self):
        if self._file_checksum is None:
            self._calculate_file_checksum()
        return self._file_checksum

    def _calculate_file_size(self):
        self._file_size = os.path.getsize(self.source_filename) if\
            self.source_filename else 0

    def _calculate_file_checksum(self):
        file_size = self.get_file_size()
        with open(self.source_filename, "rb") as file:
            checksum = 0
            a = b''
            x = 0
            while x < file_size:
                file.seek(x)
                if x > file_size - 4:
                    a = file.read(file_size % 4)
                    int_a = int(a.hex(), 16) * 2**((4 - file_size % 4)*8)
                else:
                    a = file.read(4)
                    int_a = int(a.hex(), 16)
                checksum = checksum + int_a
                x = x + 4
        checksum = checksum % 2**32
        self._file_checksum = checksum

    def get_file_segment(self):
        offset = self._file_seek
        if offset < self._file_size:
            data = self.file_handle.read(self.FILE_SEGMENT_SIZE)
            self._file_seek += self.FILE_SEGMENT_SIZE
        else:
            raise Exception("Exceeded file end")
        return offset, data

    def is_file_send_complete(self):
        return self._file_seek >= self._file_size
