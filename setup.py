from setuptools import setup


setup(
    name='python-cfdp',
    version='0.0.1',
    description='Python implementation of CCSDS File Delivery Protocol',
    license='MIT',
    python_requires='>=3.4',
    keywords='ccsds cfdp file operation',
    packages=['cfdp'],
    install_requires=[
        'pyzmq',
        'bitarray'
    ],
)
