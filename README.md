# Python CFDP

Implementation of the CCSDS File Delivery Protocol in Python. It uses
ZMQ as underlying transport layer.

## Getting Started

Clone the repository and then install via pip:

```bash
$ git clone https://gitlab.com/librecube/prototypes/python-cfdp
$ cd python-cfdp
$ pip install -e .
```

Start the receiver first from the bash:

```bash
$ cd examples
$ python monitor_zmq.py
```

Then start the sender from the bash:

```bash
$ cd examples
$ python sender.py
```
